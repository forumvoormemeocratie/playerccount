import requests
import matplotlib.pyplot as plt

def get_count():
    try:
        r = requests.get('http://www.runescape.com/player_count.js?varname=iPlayerCount&callback=jQuery000000000000000_0000000000&_=0').text
        count = r[r.index('(')+1:r.index(')')]
        return int(count)
    except:
        return -1

def update_plot():
    plt.axis([times[0], times[len(times)-1], min(counts)-500, max(counts)+500])
    graph.set_ydata(counts)
    graph.set_xdata(times)
    plt.draw()

# initialize data
counts = []
times = []
counts.append(get_count())
times.append(0)

# initialize plot
plotsize = 500 # amount of datapoints on x-axis
plt.ion()
plt.ylabel('Players')
plt.xlabel('Datapoint')
plt.title('Players online')
plt.grid(True)
graph = plt.plot(times, counts, color='black')[0]

while True:
    current = get_count()
    if current > 0:
        counts.append(get_count())
        times.append(times[len(times)-1]+1)
        if len(counts) > plotsize:
            counts.pop(0)
            times.pop(0)
        update_plot()
    plt.pause(1) # pause for 1 second
